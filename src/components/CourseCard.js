import PropTypes from 'prop-types';
//import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';

import {Link} from 'react-router-dom'

export default function CourseCard({course}){

	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component recieves information in a form of an object.
	//console.log(typeof props);

	// Destructure the "course" properties into their own variable "course" to make the code even shorter
	const {name, description, price, _id} = course;

	// use the state hook for this component to be able to store its state
	// States are used to keep track of information related to  individual components

	// Syntax
		// const [getter, setter ] = useState(initalGetterValue);

	//const [count, setCount] = useState(0);
	//const [seats, setSeats] = useState(30);
	//const [isOpen, setIsOpen] = useState(true);

	//Using the state hook returns an array with first element being a value and the second elements as a function that's used to change the value of the first element.

	//console.log(useState(0));

	//function enroll() {

		//setCount(count + 1);
		//console.log('Enrollees ' + count);
		//setSeats(seats -1)
		//console.log('Seats: ' + seats)

		// Activity
		/*if(seats > 0){
		setCount(count + 1);
		console.log('Enrollees ' + count);
     
       // Activity
		setSeats(seats -1)
		console.log('Seats: ' + seats)

		} else {
			alert("No more seats Available!")
		}*/
	//}

	// useEffect - allows us to instruct the app thta the components needs to do something after render

	//useEffect(() => {
		//if (seats === 0) {
			//setIsOpen(false);
			//alert("No more seats availbale.")
			//document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
		//}

		// Will run anytime one of the values in the array of dependencies cahnges.
	//}, [seats])

	// Three types of dependencies
		// No dependency - effect function will every time the components renders.
		// With dependency (empty array) - effect function will only run (one time) when the components mounts and unmounts.
		// With dependency = effect function will run any time one of the values in the array of dependencies changes.

	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Button  className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
	)
}


//<Card.Text>Enrollees: {count}</Card.Text>
//<Card.Text>Seats: {seats}</Card.Text>
//<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={enroll}>Enroll</Button>

// Check if the CourseCard component is getting the correc prop types
// PropTypes are used for validation information passed to a components and is a tool 
/*
CourseCard.propTypes = {
	course: PropTypes.shape({

		// Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired

	})
}*/