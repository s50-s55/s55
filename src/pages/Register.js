import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();


const registerUser = (e) => {
	e.preventDefault();

	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password : password
		
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		if (data === true) {
			Swal.fire({
				title: "Registration successful",
				icon: "success",
				text: "Welcome to Zuitt!"
			})
			navigate("/login");
		} else {
			Swal.fire({
				title: "Duplicate email found",
				icon: "error",
				text: "Please provide another email"
			})
		}
	})
}

	//console.log(email);
	//console.log(password2);
	//console.log(password1);

   //function registerUser(e){
   //
   //		e.preventDefault();
   //
   //		setEmail("");
   //		setPassword2("");
   //		setPassword1("");
   //
   //		alert("Thank you for registering!");
   //	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match

		if((email !== "" && password !== "" && password2 !== "") && (password === password2) && (password.length && password2.length >= 8) && (mobileNo.length == 11)){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, mobileNo, password, password2]);
	
	return (
		(user.id !== null) ? 
		<navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)} className="my-3">
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Last Name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNumber">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					Your Mobile Number must have 11 digit.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					Must be 8 characters or more.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 

				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
			}
		</Form>
	)
}